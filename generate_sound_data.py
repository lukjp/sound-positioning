#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

default_sample_rate = 44100
default_repeat_frequency = 2


@np.vectorize
def pink_noise_spd(f):
    if f == 0:
        return f  # keep type
    return 1/f


@np.vectorize
def red_noise_spd(f):
    if f == 0:
        return f  # keep type
    return 1/f**2


def modified_red_noise_spd(f):
    return f**2/(f**2 + 1000**2)/(f**2 + 1000**2)/(f**2 + 10000**2)


@np.vectorize
def band_noise(f):
    if 18000 < f < 19000:
        return 1
    return 0


@np.vectorize
def peaks(f):
    if f % 100 == 0 and f != 0:
        return 1
    return 0


def white_noise(f):
    return np.full_like(f, 1)


def generate_noise_sound_data(sample_rate, repeat_frequency, noise_spd, rng_seed):
    sample_count = sample_rate // repeat_frequency
    f_count = sample_count // 2 + 1

    if repeat_frequency * (sample_count // 2) * 2 != sample_rate:
        raise ValueError('sampleRate must be divisable by 2 * repeat_frequency')

    f_array = np.linspace(0, sample_rate // 2, f_count)
    rg = np.random.default_rng(rng_seed)
    noise_fft = rg.normal(size=f_count) + 1j * rg.normal(size=f_count)
    noise_fft = noise_fft * noise_spd(f_array)
    return np.fft.irfft(noise_fft)


def generate_sound_data_default(rng_seed=0):
    return generate_noise_sound_data(default_sample_rate, default_repeat_frequency,
                                     band_noise, rng_seed)
