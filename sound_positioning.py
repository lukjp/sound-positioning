#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import sounddevice
import time
import queue

import generate_sound_data
import locator
import sound_data

record_count = 1
hist_length_seconds = 20


#positions = np.array([[0.0, 0.0],
#                      [3.1, 0.0],
#                      [0.0, 3.5],
#                      [3.1, 3.5]])

positions = np.array([[0.15],
                      [-0.15]])

used_speakers = [0, 1]

max_distance = 0.5

n_channels = positions.shape[0]
dims = positions.shape[1]
speed_of_sound = 343
pos_tol = 0.5

fs = generate_sound_data.default_sample_rate
record_time = record_count / generate_sound_data.default_repeat_frequency
f_arr = np.arange(fs * record_time / 2 + 1) / record_time

samples_per_period = fs // generate_sound_data.default_repeat_frequency
t_arr = np.arange(-samples_per_period + 1, 1) / fs

sound_references = [generate_sound_data.generate_sound_data_default(i)
                    for i in range(n_channels)]

hist_count = int(round(hist_length_seconds / record_time)) + 1
history_time_points = np.linspace(-hist_length_seconds, 0, hist_count)

q = queue.Queue()

loc = locator.locator_modulo(
    positions[used_speakers], speed_of_sound,
    1 / generate_sound_data.default_repeat_frequency,
    max_distance, pos_tol)



def audio_callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    q.put(indata[:, 0])


stream = sounddevice.InputStream(
    samplerate=fs, blocksize=int(fs * record_time), callback=audio_callback,
    channels=1)
stream.start()


def get_data():
    data = []
    try:
        while True:
            data.append(q.get(block=False))
    except queue.Empty:
        pass
    return data


fig = plt.figure()
(ax_fft, ax_quality), (ax_corr, ax_pos) = fig.subplots(2, 2)

ax_fft.grid(True)
dat = get_data()

while len(dat) == 0:
    dat = get_data()
dat = dat[-1]

sd = sound_data.SoundData(loc.locate_safe, dat, sound_references, fs)

psd_line, = ax_fft.plot(f_arr, sd.get_psd(), '-', lw=1)
ax_fft.grid(True)
ax_fft.set_ylim([0, 7])

corr_lines = ax_corr.plot(t_arr, np.transpose(sd.get_correlations()),
                          '-', lw=1)
ax_corr.grid(True)
ax_corr.set_ylim([-5, 5])

hist = np.zeros((hist_count, dims + 1))
distance_hist_lines = ax_pos.plot(history_time_points, hist, '-', lw=1)
ax_pos.set_ylim([-max_distance, max_distance])

quality_hist = np.zeros((hist_count, n_channels))
quality_hist_lines = ax_quality.plot(history_time_points, quality_hist, '-', lw=1)
ax_quality.set_ylim([0, 150])


def updateAnimation(i):
    data = get_data()
    for dat in data:
        sd.set_data(dat)
        psd_line.set_data(f_arr, sd.get_psd())
        quality_hist[:] = np.roll(quality_hist, -1, axis=0)
        quality_hist[-1, :] = sd.get_signal_strength()
        corrs = sd.get_correlations()
        for i in range(n_channels):
            quality_hist_lines[i].set_data(history_time_points, quality_hist[:, i])
            corr_lines[i].set_data(t_arr, np.log(corrs[i]))
        
        hist[:] = np.roll(hist, -1, axis=0)
        pos_time = sd.get_location()
        if pos_time is None:
            hist[-1] = hist[-2]
        else:
            hist[-1,:-1] = pos_time[0]
            hist[-1,-1] = pos_time[1]
        for i in range(dims + 1):
            distance_hist_lines[i].set_data(history_time_points, hist[:, i])
    return psd_line, quality_hist_lines, corr_lines, distance_hist_lines


ani = animation.FuncAnimation(fig, updateAnimation, None, interval=1,
                              blit=False)
fignum = fig.number
plt.show()
# stream.stop()
# stream.close()
