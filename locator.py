#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import scipy.optimize


class locator():
    def __init__(self, speaker_positions, speed_of_sound, pos_tol=None):
        self.channels = speaker_positions.shape[0]
        self.dimensions = speaker_positions.shape[1]
        if self.dimensions > self.channels - 1:
            raise ValueError('At least dimensions + 1 channels are needed')
        self.speaker_positions = speaker_positions
        self.center = np.average(speaker_positions, axis=0)
        self.pos_tol = pos_tol
        self.speed_of_sound = speed_of_sound

    def calc_times(self, position, time_offset):
        dists2 = np.sum((self.speaker_positions - position)**2, axis=1)
        return np.sqrt(dists2) / self.speed_of_sound + time_offset

    def locate(self, times):
        if len(times) != self.channels:
            raise ValueError('len(times) must be the number of channels')

        def error(pos_time):
            time_errors = self.calc_times(pos_time[:-1], pos_time[-1]) - times
            return np.linalg.norm(time_errors)

        pos_time_0 = np.append(self.center, np.average(times))
        res = scipy.optimize.minimize(error, pos_time_0)
        if not res.success and res.message != 'Desired error not necessarily achieved due to precision loss.':
            print('not converged')
            print(res)
            return None
        if self.pos_tol is not None and (res.fun * self.speed_of_sound
                                         > self.pos_tol**2):
            print('Convergence not good enough')
            return None
        return (res.x[:-1], res.x[-1])


class locator_modulo(locator):
    def __init__(self, speaker_positions, speed_of_sound, period, max_dist,
                 pos_tol=None):
        if max_dist / speed_of_sound > period / 2:
            raise ValueError('max_delta must be smaller than period / 2')
        super().__init__(speaker_positions, speed_of_sound, pos_tol)
        self.period = period
        self.max_delta = max_dist / speed_of_sound

    def locate_safe(self, times):
        times = np.array(times)
        min_t = times[0] - self.max_delta
        times[1:] = np.mod(times[1:] - min_t, self.period) + min_t
        if np.any(times > times[0] + 2 * self.max_delta):
            print('Too large time differences')
            return None
        return self.locate(times)
