#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

import generate_sound_data

data = generate_sound_data.generate_sound_data_default()
t_array = np.linspace(0, 1 / generate_sound_data.default_repeat_frequency,
                      len(data), endpoint=False)

f_array = np.linspace(0, generate_sound_data.default_sample_rate // 2,
                      generate_sound_data.default_sample_rate // (2 * generate_sound_data.default_repeat_frequency) + 1)
psd = np.log(np.abs(np.fft.rfft(data)))


fig, axes = plt.subplots(2)
axes[0].plot(t_array, data)
axes[1].plot(f_array, psd)
axes[1].set_ylim([-40, 0])

plt.show()
