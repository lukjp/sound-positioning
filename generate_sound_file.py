#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import wave
import struct
import numpy as np

import generate_sound_data

max_amplitude = 32000
total_time = 200
n_channels = 2

repeats = total_time * generate_sound_data.default_repeat_frequency


def store_sound_data(fname, data, repeats=1):
    if data.ndim == 1:
        channels = 1
        data = np.array([data])
    elif data.ndim == 2:
        if data.shape[0] == 1:
            channels = 1
        elif data.shape[0] == 2:
            channels = 2
        else:
            raise Exception()
    else:
        raise Exception()
    wavef = wave.open(fname, 'w')
    wavef.setnchannels(channels)
    wavef.setsampwidth(2)
    wavef.setframerate(generate_sound_data.default_sample_rate)
    wfdata = b''
    for sample in np.transpose(data):
        for channel in sample:
            wfdata += struct.pack('h', channel)
    for i in range(repeats):
        wavef.writeframesraw(wfdata)
    wavef.writeframes(b'')
    wavef.close()


noise = np.array([generate_sound_data.generate_sound_data_default(seed)
                  for seed in range(n_channels)])
noise *= max_amplitude / np.max(np.abs(noise))
noise = noise.astype(int)

if n_channels <= 2:
    fname = 'test_sound_' + str(n_channels) + 'channels.wav'
    store_sound_data(fname, noise, repeats)
else:
    for i, channel in enumerate(noise):
        fname = 'test_sound_ch_nr' + str(i) + '.wav'
        store_sound_data(fname, channel, repeats)
