#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

class SoundData:
    def __init__(self, locate_func, data, sound_references, sample_rate):
        self.locate_func = locate_func
        self.set_data(data)
        self.references_fft = np.conj(np.fft.fft(sound_references, axis=-1))
        self.time_per_idx = 1 / sample_rate
        self.fft = None
        self.correlations = None
        self.shifts = None

    def set_data(self, data):
        self.data = data
        self.fft = None
        self.correlations = None
        self.shifts = None

    def get_fft(self):
        if self.fft is None:
            self.fft = np.fft.fft(self.data)
        return self.fft

    def get_psd(self):
        fft = self.get_fft()
        return np.log(abs(fft[:len(fft) // 2 + 1]))

    def get_correlations(self):
        if self.correlations is None:
            self.correlations = np.abs(np.fft.ifft(self.references_fft
                                                   * self.get_fft()))
        return self.correlations

    def get_shifts(self):
        if self.shifts is None:
            self.shifts = np.argmax(self.get_correlations(), axis=-1)
        return self.shifts

    def get_rel_times(self):
        return self.time_per_idx * self.get_shifts()

    def get_signal_strength(self):
        corrs = self.get_correlations()
        noises = np.sqrt(np.linalg.norm(corrs, axis=-1) / corrs.shape[1])
        signals = corrs[np.arange(len(corrs)), self.get_shifts()]
        return signals / noises

    def get_location(self):
        return self.locate_func(self.get_rel_times())